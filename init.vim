
set tabstop=4 softtabstop=4 shiftwidth=4
set expandtab
set smartindent
set nu rnu "set number relativenumber
set encoding=utf-8
set clipboard=unnamedplus
set noerrorbells
set splitbelow splitright
set nohlsearch
set nowrap
set incsearch
set scrolloff=2
set sidescroll=0
set colorcolumn=80
set foldlevel=99

highlight ColorColumn ctermbg=2 
language en_US
syntax enable
filetype plugin indent on

" Highlight search toggle
nmap <F3> :set hls! <cr>

" VIM PRESENTATION
"nmap <F5> :set relativenumber! number! showmode! showcmd! hidden! ruler!<CR>

call plug#begin()
  Plug 'preservim/nerdtree'
  Plug 'preservim/nerdcommenter'
  Plug 'neovim/nvim-lspconfig'

  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
  Plug 'kyazdani42/nvim-web-devicons'
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim'

  " Colors
  Plug 'gruvbox-community/gruvbox'  " Required for colored lsp messages
  Plug 'arcticicestudio/nord-vim' " :colorscheme nord
  Plug 'rakr/vim-one'   " :set background light|dark & :colorscheme one
  Plug 'sonph/onehalf', { 'rtp': 'vim' } " :colorscheme onehalflight|onehalfdark
  Plug 'lifepillar/vim-solarized8' " :set background light|dark & :colorscheme solarized8

  " Git
  Plug 'tpope/vim-fugitive'

  " Folding
  Plug 'pseewald/vim-anyfold'

  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'tpope/vim-surround'
  Plug 'dominikduda/vim_current_word'
call plug#end()

let mapleader = " "


" Telescope commands
" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

nmap <leader>co <Plug>NERDCommenterToggle
vmap <leader>co <Plug>NERDCommenterToggle<CR>gv
nmap <F2> :NERDTreeToggle<CR>

nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gr <Plug>(coc-references)

" gruvbox
let g:gruvbox_contrast_dark = 'hard'
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
let g:gruvbox_insert_selection='0'

set termguicolors     " enable true colors support

" nord theme
"set background=dark
"colorscheme nord

" one half colors
"set t_Co=256
"set cursorline
"colorscheme onehalflight
"let g:airline_theme='onehalfdark'
"" lightline
"let g:lightline = { 'colorscheme': 'onehalfdark' }

" solarized8
set background=dark
autocmd vimenter * ++nested colorscheme solarized8

" Folding
autocmd Filetype * AnyFoldActivate
"set foldlevel=0  " close all folds
" or
"set foldlevel=99 " Open all folds

"let g:airline_theme='base16_nord'
"let g:airline_theme='minimalist'
let g:airline_theme='solarized'

" COC autocomplete pick

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" IndentLine {{
let g:indentLine_char = ''
let g:indentLine_first_char = ''
let g:indentLine_showFirstIndentLevel = 1
let g:indentLine_setColors = 0
" }}






let g:vim_current_word#highlight_current_word = 0
"let g:vim_current_word#highlight_twins = 1









if v:version < 700 || !has('python3')
    func! __BLACK_MISSING()
        echo "The black.vim plugin requires vim7.0+ with Python 3.6 support."
    endfunc
    command! Black :call __BLACK_MISSING()
    command! BlackUpgrade :call __BLACK_MISSING()
    command! BlackVersion :call __BLACK_MISSING()
    finish
endif

if exists("g:load_black")
  finish
endif

let g:load_black = "py1.0"
if !exists("g:black_virtualenv")
  if has("nvim")
    let g:black_virtualenv = "~/.local/share/nvim/black"
  else
    let g:black_virtualenv = "~/.vim/black"
  endif
endif
if !exists("g:black_fast")
  let g:black_fast = 0
endif
if !exists("g:black_linelength")
  let g:black_linelength = 88
endif
if !exists("g:black_skip_string_normalization")
  if exists("g:black_string_normalization")
    let g:black_skip_string_normalization = !g:black_string_normalization
  else
    let g:black_skip_string_normalization = 0
  endif
endif
if !exists("g:black_quiet")
  let g:black_quiet = 0
endif
if !exists("g:black_target_version")
  let g:black_target_version = ""
endif

function BlackComplete(ArgLead, CmdLine, CursorPos)
  return [
\    'target_version=py27',
\    'target_version=py36',
\    'target_version=py37',
\    'target_version=py38',
\    'target_version=py39',
\  ]
endfunction

command! -nargs=* -complete=customlist,BlackComplete Black :call black#Black(<f-args>)
command! BlackUpgrade :call black#BlackUpgrade()
command! BlackVersion :call black#BlackVersion()











" Python LSP
"lua << EOF


"lua << EOF
  "local nvim_lsp = require'nvim_lsp'
  "-- Disable Diagnostcs globally
  "vim.lsp.callbacks["textDocument/publishDiagnostics"] = function() end
"EOF

"require'lspconfig'.pyright.setup{}
"EOF
lua << EOF
require'lspconfig'.pyright.setup{}
EOF
