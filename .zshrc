source /Users/sergiosolano/antigen.zsh

alias dircolors='gdircolors'

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

antigen bundle joel-porquet/zsh-dircolors-solarized.git

# Load the theme.
antigen theme robbyrussell

# Tell antigen that you're done.
antigen apply

export LANG="en_US.UTF-8"
